<html>
    <head> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../style/style.css" rel="stylesheet" type="text/css">
    <title itemprop="name">Scarhunt 2022</title>

</head>
    <body>
<div class="header"><div><a href="../" class="icon-btn"><img class="icon" width="24" src="../img/arrow-left.svg"/></a></div><div><h2>SCARHUNT</h2></div><div><a href="../map " class="icon-btn"><img class="icon" width="24" src="../img/map.svg"/></a></div></div>
<div class="content">
<div class="filter-container"><div class="filter"><div><img class="icon" width="24" src="../img/map-marker.svg"/>locaties</div><div><img class="icon" width="24" src="../img/run.svg"/>Opdrachten</div><div><img class="icon" width="24" src="../img/book-open-variant.svg"/>Vragen</div></div></div>
<?php

require '../vendor/autoload.php';

use Google\Cloud\Firestore\FirestoreClient;

$firestore = new FirestoreClient([
  'keyFile' => json_decode(file_get_contents('../scarhunt-2022-89db9d32a5e2.json'), true)
]);
$firestore = new FirestoreClient([
  'keyFilePath' => '../scarhunt-2022-89db9d32a5e2.json'
]);
$firestore = new FirestoreClient([
  'projectId' => 'scarhunt-2022'
]);

$collectionReference = $firestore->collection('opdrachten');
$documentReference = $collectionReference->document($userId);
$snapshot = $documentReference->snapshot();

echo "Hello " . $snapshot['id'];

for($i=1;$i < 7;$i++){
    if($i > 1){
    };
    echo'
    <div class="card">
<div class="card-head">
    <div class="head-avatar"><img class="icon" width="24" src="../img/map-marker.svg"/></div>
    <div class="card-title">
  <h2 class="">Locatie '.$i.'</h2>
  <h3 class=""></h3>
</div>
      <div class="head-action"><a class="icon-btn"><img alt="Favoriet" class="icon one" width="24" src="../img/heart-outline.svg"/><img alt="Favoriet" class="icon two" width="24" src="../img/heart.svg"/></a></div>
</div>
<div class="card-content">
<div class="card-image"><a href=""><img width="350" src="https://i2.wp.com/thalmaray.co/wp-content/uploads/2016/08/bunker-miroirs-anonyme-dunkerque-2.jpg?resize=700%2C437"/></a></div>
</div>
<div class="footer-action"> <form> <label for="upload" class="upload-btn"><img alt="Favoriet" class="icon" width="24" src="../img/camera.svg"/>upload foto
</label><input type="file" id="upload" name="upload" accept="image/" onchange="loadFile(event,\'opdracht 1\');"></form> </div>
</div>
  ';}

  for($i=1;$i < 7;$i++){
    if($i > 1){
    };
    echo'
    <div class="card">
<div class="card-head">
    <div class="head-avatar"><img class="icon" width="24" src="../img/run.svg"/></div>
    <div class="card-title">
  <h2 class="">Opdracht '.$i.'</h2>
  <h3 class=""></h3>
</div>
      <div class="head-action"><a class="icon-btn"><img alt="Favoriet" class="icon one" width="24" src="../img/heart-outline.svg"/><img alt="Favoriet" class="icon two" width="24" src="../img/heart.svg"/></a></div>
</div>
<div class="card-content">
<h2>Maak een menselijke piramide van tenminste 6 personen</h2>
</div>
<div class="footer-action"> <form> <label for="upload2" class="upload-btn"><img alt="Favoriet" class="icon" width="24" src="../img/camera.svg"/>upload foto
</label><input type="file" id="upload2" name="upload" accept="image/" onchange="loadFile(event,\'opdracht 12\',\'Maak een menselijke piramide van tenminste 6 personen\');"></form> </div>
</div>
  ';}

  for($i=1;$i < 7;$i++){
    if($i > 1){
    };
    echo'
    <div class="card">
<div class="card-head">
    <div class="head-avatar"><img class="icon" width="24" src="../img/book-open-variant.svg"/></div>
    <div class="card-title">
  <h2 class="">Vraag '.$i.'</h2>
  <h3 class=""></h3>
</div>
      <div class="head-action"><a class="icon-btn"><img alt="Favoriet" class="icon one" width="24" src="../img/heart-outline.svg"/><img alt="Favoriet" class="icon two" width="24" src="../img/heart.svg"/></a></div>
</div>
<div class="card-content">
<h2>Hoeveel is 3x3</h2>
</div>
<div class="footer-action"> <form> <label for="upload-text" class="upload-btn"><img alt="Favoriet" class="icon" width="24" src="../img/comment-text-multiple.svg"/>upload antwoord
</label><input  type="button" id="upload-text" name="upload-text" ></form> </div>
</div>
  ';}
  ?>
       </div> 
       <div id="upload-overlay">
       <div>
         <div>
       <h2 id="upload-title"></h2>
       <h4 id="upload-subtitle"></h4>
      </div>
      <a onclick="closePreview();" class="icon-btn"><img alt="Favoriet" class="icon one" width="24" src="../img/close.svg"/><img alt="Favoriet" class="icon two" width="24" src="../img/heart.svg"/></a>
      </div>
       <div class="preview-image">
      <img id="preview" src="#" alt="your image" />
    </div>
    <div>
       <button> Versturen</button>
      </div>
     </div>
       <script src="../js/app.js"></script>
   </body>
    </html>

