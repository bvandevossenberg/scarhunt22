<html>
    <head> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style/style.css" rel="stylesheet" type="text/css">
    <title itemprop="name">Scarhunt 2022</title>

</head>
    <body>
    <div class="header"><div><a href="" class="icon-btn"><img class="icon" width="24" src="img/home.svg"/></a></div><div><h2>SCARHUNT</h2></div><div><a href="map " class="icon-btn"><img class="icon" width="24" src="img/map.svg"/></a></div></div>
<div class="content">
<?php for($i=1;$i < 7;$i++){
  if($i>1){
        $overlay = '<div class="lock-overlay" id="overlay'.$i.'"><div><img alt="regio gesloten" class="icon" width="140" src="img/lock.svg"/></div>
        <div class="progress-bar">
            <div>District '.($i - 1).'</div>
            <div class="bar-container">
                <div class="bar"><div class="inner-bar"></div> </div>
                <div>10/15</div>
            </div>
            </div> </div>';}
    echo'
    <div class="card" id="card'.$i.'">
    <a href="district">
  <div class="card-head">
      <div class="head-avatar"><!--<img class="icon" width="24" src="img/lock.svg"/>--></div>
      <div class="card-title">
    <h2 class="">District '.$i.'</h2>
    <h3 class="">The first steps</h3>
</div>
        <div class="head-action"><span id="countdown'.$i.'">00:00:00</span></div>
  </div>
  <div class="card-content">
  <div class="card-progress"> 
  <div class="progress-circle small" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="--value:65"><div class="progress-circle-inner"><img alt="Doe opdrachten" class="icon" width="24" src="img/run.svg"/></div></div>
  <div class="progress-circle" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="--value:65"><div class="progress-circle-inner"><img alt="locaties" class="icon" width="52" src="img/map-marker.svg"/></div></div>
  <div class="progress-circle small" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="--value:65"><div class="progress-circle-inner"><img alt="Vragen" class="icon" width="24" src="img/book-open-variant.svg"/></div></div>
</div>
  </div>
  </a>
    '.$overlay.'
  </div>
  ';}
  ?>
       </div> 
   </body>
   <script>
// Set the date we're counting down to
function countdown(startdate,enddate,id){
var endDate = new Date(enddate).getTime();
var startDate = new Date(startdate).getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = endDate - now;

  // Time calculations for days, hours, minutes and seconds
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById('countdown'+id).innerHTML = ('0' + hours).slice(-2) + ":"
  +('0' + minutes).slice(-2) + ":" + ('0' + seconds).slice(-2);

  // If the count down is finished, write some text
  /*
  if ( (startDate - now) <0) {
    document.getElementById('overlay'+id).style.display = "none";
  }
  if ((endDate - now) <0) {
    document.getElementById('card'+id).style.display = "none";
  }*/
}, 1000);
}
countdown("nov 5, 2021 16:00:00","nov 8, 2021 16:00:00",1,);
countdown("nov 5, 2021 16:25:00","nov 8, 2021 16:30:00",2);
countdown("nov 5, 2021 16:30:00","nov 8, 2021 16:35:00",3);
countdown("nov 5, 2021 16:30:00","nov 8, 2021 16:35:00",4);
countdown("nov 5, 2021 16:30:00","nov 8, 2021 16:35:00",5);
countdown("nov 5, 2021 16:30:00","nov 8, 2021 16:35:00",6);
</script>
    </html>

