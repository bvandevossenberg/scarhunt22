var loadFile = function (event, title, subtitle) {
  document.getElementById("upload-overlay").style.display = "flex";
  var output = document.getElementById("preview");
  document.getElementById("upload-title").innerHTML = title;
  document.getElementById("upload-subtitle").innerHTML = subtitle;
  output.src = URL.createObjectURL(event.target.files[0]);
  output.onload = function () {
    URL.revokeObjectURL(output.src); // free memory
  };
};
