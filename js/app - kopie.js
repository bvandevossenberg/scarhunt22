firebase.initializeApp({
  apiKey: "AIzaSyAxihYH62mXQfGgYgWV_VtjGne1Fdv97Ow",
  authDomain: "scarhunt-2022.firebaseapp.com",
  projectId: "scarhunt-2022",
  storageBucket: "scarhunt-2022.appspot.com",
  messagingSenderId: "894558587587",
  appId: "1:894558587587:web:2516493d7f252a2b59f076",
  measurementId: "G-EDVZF2B5ZH",
});

const db = firebase.firestore();

const team = 1;
const teamData = getTeamData(team)
getData(1);

function getTeamData(team) {
  const teamData =new Array;
  db.collection("antwoorden")
    .where("team", "==", team)
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        teamData.push(doc.data());
        console.log(`${doc.id} => ${doc.data()['id']}`);
      });
    });
    return teamData;
}

function notDone(id){
  const output = teamData.includes(id);
  return output;
}

function getData(region) {
  let icon ="";
  let iconUpload ="";
  let contentBlock ="";
  db.collection("opdrachten")
    .where("region", "==", 1)
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        const content = doc.data()["content"];
        const id = doc.data()["id"];
        const image = doc.data()["image"];
        const cat = doc.data()["cat"];
        switch(cat){
          case 'doe' : icon = 'run'; iconUpload = 'camera'; contentBlock = '<h2>' +content +'</h2>';
          break;
          case 'location' : icon = 'map-marker'; iconUpload = 'camera'; contentBlock = '<div class="card-image"><a href=""><img width="350" src="'+image+'"/></a></div>';
          break;
        }
        console.log(notDone(id));
        document.getElementById("cards").innerHTML +=
          '<div class="card"><div class="card-head"> <div class="head-avatar"><img class="icon" width="24" src="../img/'+
          icon+'.svg"/></div> <div class="card-title"><h2 class="">Opdracht ' +
          id +
          '</h2><h3 class=""></h3>      </div><div class="head-action"><a class="icon-btn"><img alt="Favoriet" class="icon one" width="24" src="../img/heart-outline.svg"/><img alt="Favoriet" class="icon two" width="24" src="../img/heart.svg"/></a></div>      </div><div class="card-content">'+contentBlock+'</div><div class="footer-action"> <form> <label for="upload'+
          id+'" class="upload-btn"><img alt="Favoriet" class="icon" width="24" src="../img/'+
          iconUpload+'.svg"/>upload foto</label><input type="file" id="upload'+
          id+'" name="upload" accept="image/" onchange="loadFile(event,\'opdracht '+
          id+'\',\'' +
          content +
          "');\"></form> </div></div>";
      });
    });
}

function addData(id, team, antwoord) {
  id = 12;
  team = 1;
  antwoord = "test";
  db.collection("antwoorden")
    .add({
      id: id,
      team: team,
      antwoord: antwoord,
    })
    .then((docRef) => {
      console.log("Document written with ID: ", docRef.id);
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
}

var loadFile = function (event, title, subtitle) {
  document.getElementById("upload-overlay").style.display = "flex";
  var output = document.getElementById("preview");
  document.getElementById("upload-title").innerHTML = title;
  document.getElementById("upload-subtitle").innerHTML = subtitle;
  output.src = URL.createObjectURL(event.target.files[0]);
  output.onload = function () {
    URL.revokeObjectURL(output.src); // free memory
  };
};
